import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { ProductListComponent } from './product-list/product-list.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { AuthGuardGuard } from '../shared/services/auth-guard/auth-guard.guard';


const routes: Routes = [
  {path:'admin' , component : AdminComponent,canActivate:[AuthGuardGuard],
  children : [
    {path:'productlist', component : ProductListComponent},
    {path :'customerlist',component : CustomerListComponent}
  ]},

  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
