import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/shared/services/product/product.service';
import { Product } from 'src/app/models/Product';
import { CategoryService } from 'src/app/shared/services/category/category.service';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  constructor(public productService : ProductService,
    public categoryService : CategoryService ) { }

  product : Product;
  ModalTitle:string;
  ActivateAddEditProductComp:boolean=false;
  checkModal : boolean=true;
  totalRecords: string;
  page : number=1;
  productobj : Product;
  categoryIdInput : string ="";
  AddProductModalOpen : boolean =false;
  UpdateProductModalOpen : boolean =false;





  ngOnInit(): void {
  
      this.loadCategoriesList();
      this.productService.fetchProductList();
   
  }

  loadCategoriesList(){
    this.categoryService.fetchCategoriesList();
  }




  onUpdateProductFromList(product : Product){
    this.product=product;
    this.ModalTitle="Update Product!!!";  
    this.UpdateProductModalOpen = true; 
  }








  addProductButtonClick(){
    this.product={
      productId:0,
      productName:"",
      price:0,
      categoryId:0,
      quantity:0,
      productImage:"",
      productStatus:""
    }
    this.ModalTitle="Add Product!!!";
    this.AddProductModalOpen = true;
    this.refreshProductList();
  }







  addProduct(obj : Product){
    let categoryId = this.categoryIdInput.toString();
    let catId = categoryId.slice(0,1);
    obj.categoryId = parseInt(catId);
    this.productService.postProduct(obj).subscribe(
      {
        next : (result)=>{
           console.log(result);
        }
      });
      this.refreshProductList();
  }







  updateProduct(obj : Product){
    let categoryId = this.categoryIdInput.toString();
    let catId = categoryId.slice(0,1);
    obj.categoryId = parseInt(catId);
    this.productService.updateProductById(obj).subscribe(
      {
        next : (result)=>{
           console.log(result);
        }
      });

    this.refreshProductList();
  }






  closeClick(){
      this.refreshProductList();
  }







  refreshProductList(){
    this.productService.fetchProductList();
  }








  onDelete(product: Product){
      product.productStatus ="NonApproved";
    this.productService.deleteProductById(product).subscribe(
      {
        next : (result)=>{
           console.log(result);
        }
      });
  }








}
