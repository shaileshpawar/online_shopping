import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/shared/services/auth/authentication.service';
import { NavbarComponent } from 'src/app/header/navbar/navbar.component';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({});
  success : string = "";
  error : string = "";
  formElementslogin = ['email','password'];
  
 
  
  
  constructor(private router :Router,private authService : AuthenticationService) { }

  ngOnInit(): void {
  }


  setForm(form: FormGroup) {
    this.loginForm = form;
  }


  submitForm() {
    
    this.authService.login(this.loginForm.value).subscribe(
      {
        next : (result  ) => {
        console.log(result);
          this.authService.saveTokenToLocalStorage(result.toString());
          console.log(localStorage.getItem('userName'));
          
          this.success = "You are login successfully!!";
          this.error = undefined;
        
          this.router.navigate(['']);
         
          console.log("after on init");
          
         

        },
        error : (response : HttpErrorResponse) => {
          console.log(response);
          this.success = undefined;
          this.error = response.error;
        }
      })
  }







}


