import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UserService } from 'src/app/shared/services/user/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  signUpForm: FormGroup = new FormGroup({});
  
  constructor(private userService : UserService,private router :Router) { }
  
  formElements = ['customerName','email','password','contactNo','gender','address'];

  ngOnInit(): void {
  }

  
  setForm(form: FormGroup) {
    this.signUpForm = form;
  }



  submitForm() {
    this.userService.signup(this.signUpForm.value).subscribe(
      {
        next : (result)=>{
           console.log(result);
          
           this.signUpForm.reset();  
           this.navigateToLoginPage();
        }
      });
  }


  navigateToLoginPage(){
    this.router.navigate(['login']);
  }








}
