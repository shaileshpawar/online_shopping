import { Directive, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';


@Directive({
  selector: '[appSignup]'
})
export class SignupDirective {

  @Output() setForm: EventEmitter<FormGroup> = new EventEmitter();
  @Input() formElements: string[] = [];
  constructor() {
  }

  ngOnInit() {
    let signUpForm = new FormGroup({});
    
    this.formElements.forEach((element) => {
      signUpForm.addControl(element, new FormControl())
    })

    this.setForm.emit(signUpForm);
  }

}
