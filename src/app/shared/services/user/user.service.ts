import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/Operators';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  postUserUrl : string = 'http://localhost:61205/api/Customers/PostCustomer';
  //loginUserUrl : string = 'http://localhost:61205/api/Customers/PostCustomer';
 
  constructor(private http : HttpClient) { }

  signup(userData:any){
    console.log(userData);
    return this.http.post(this.postUserUrl,userData);
  }



  // login(loginData:any){
  //   return this.http.post(this.loginUserUrl,loginData).pipe(
  //     map((result : loginResponse )=>{
  //       this.saveTokenToLocalStorage(result.token);
  //       return <loginResponse>result
  //     })
  //   )
  // }

  // private saveTokenToLocalStorage(token:string){
  //   localStorage.setItem('token' ,"Bearer "+ token)
  // }

  // public getToken(){
  //   return localStorage.getItem('token') ? localStorage.getItem('token') : "";
  // }



}



// interface loginResponse{
//   token : string;
//   message : string;
// }