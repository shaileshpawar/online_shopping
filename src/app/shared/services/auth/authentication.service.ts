import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as jwt_decode from "jwt-decode";
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  loginUserUrl : string = 'http://localhost:61205/api/login/userLogin';
  public userName : string ="";
  public userRole : string ="";
  public navVar : any;

  constructor(private http : HttpClient) { }

  login(loginData:any){
    return this.http.post(this.loginUserUrl,loginData);
     
  }


  public saveTokenToLocalStorage(token:string){
    localStorage.setItem('token' ,"Bearer "+ token);

    var decodesToken = jwt_decode(token);  
    this.userName = decodesToken.unique_name;
    localStorage.setItem('userName',this.userName);
    this.userRole = decodesToken.role;
    localStorage.setItem('userRole',this.userRole);
    this.navVar = localStorage.getItem('userRole');
   
   
  }


  public getToken(){
    return localStorage.getItem('token') ;
  }

  public removeToken(){
     localStorage.clear();
    
  }

  isAuthenticated() : boolean {
      if(localStorage.getItem('userRole') == 'Admin')
      {
        return true;
      }
      return false;
  }





}








