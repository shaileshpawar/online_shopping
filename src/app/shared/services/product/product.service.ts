import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from 'src/app/models/Product';
import { AuthenticationService } from '../auth/authentication.service';
import { Category } from 'src/app/models/Category';



@Injectable({
  providedIn: 'root'
})
export class ProductService {

  getAllProductsUrl : string ='http://localhost:61205/api/products/getAllProducts'; 
  postProductsUrl : string ='http://localhost:61205/api/products/postProduct';
  deleteProductByIdUrl : string ='http://localhost:61205/api/products/deleteProduct';
  updateProductByIdUrl : string ='http://localhost:61205/api/products/updateProduct';
  getAllCategoriesUrl : string ='';
  public productlist : Product[] =[];
  public arrayLength:number;
 



  constructor(private http : HttpClient ,
              private auth : AuthenticationService) { 

    this.fetchProductList();
   
  }

 fetchProductList(){
    // this.http.get<Product[]>(this.getAllProductsUrl,
    //   {
    //     headers : {
    //       'authorization' : this.auth.getToken()
    //     }
    //   }).subscribe(data => this.productlist = data);


    this.http.get<Product[]>(this.getAllProductsUrl).subscribe(data => this.productlist = data);
    this.arrayLength = this.productlist.length;
  
   }

  getAllProducts() : Observable<Product[]> {
  
    return this.http.get<Product[]>(this.getAllProductsUrl)
      
  }


  postProduct(product : Product){
 
    
    return this.http.post<Product>(this.postProductsUrl,product);
  }

  deleteProductById(product : Product){
      console.log(product);
      
    return this.http.post(this.deleteProductByIdUrl,product);
  }

  updateProductById(product : Product){
    console.log(product);
    
    return this.http.put(this.updateProductByIdUrl,product);
  }
 
  






  

}
