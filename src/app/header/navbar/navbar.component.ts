import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ProductService } from 'src/app/shared/services/product/product.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AuthenticationService } from 'src/app/shared/services/auth/authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {


  searchString : string;
  isLogin : boolean = false;
  userName : string ="";
  userRole : string ="";
  navVar : any;
 


  constructor(private  productService : ProductService,private router : Router,
                public authService : AuthenticationService ,private route : ActivatedRoute) {   }
     
    
  ngOnInit(): void {
      
    this.userName = localStorage.getItem('userName');
    this.userRole = localStorage.getItem('userRole');
    console.log("name="+this.userName);
    console.log("role="+this.userRole);
    
    
    
  }

  searchStringSubmit(){
      this.router.navigate([''],//to navigate we used router,in navigate pass url and params querystring name value pair
      {
        queryParams :{
         'searchby' : this.searchString
        }
      })
      this.searchString = null;
  }


  logout(){
    this.authService.removeToken();
    localStorage.clear();
    this.ngOnInit();
    this.router.navigate(['']);
  }

 

  

 
    
   
  
 



}
